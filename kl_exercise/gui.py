__author__ = "Klevin Laforteza"
__email__ = "lafortezaklevin@gmail.com"
__version__ = "1.0"

import maya.cmds as mc
import os
from PySide import QtCore, QtGui
import logging

log = logging.getLogger(__name__)
log.setLevel(logging.WARNING)

ICON_PATH = os.path.abspath(os.path.join(__file__, os.pardir, "icons"))
INIT_MESSAGE = "Weta Digital - Motion R&D Pipeline TD - Python QT Challenge"


class KLWindow(QtGui.QMainWindow):

    def __init__(self):
        super(KLWindow, self).__init__()
        log.debug("Initialising...")
        self.create_window()
        self.create_ui()
        self.show()

    def create_window(self):
        """
        Creates the object's window
        :return:
        """
        log.debug("Creating window.....")
        # Delete window if it already exists
        window_name = "KLWindow"
        if mc.window(window_name, query=True, exists=True):
            mc.deleteUI(window_name, window=True)

        # Set object attributes
        self.setWindowTitle("KL Exercise {}".format(__version__))
        self.setObjectName(window_name)
        self.resize(500, 300)

    def create_ui(self):
        """
        Creates the UI's layout and widgets
        :return:
        """
        # --------------------------------------------------------------------------------------------------------------
        # Create QT widgets
        # --------------------------------------------------------------------------------------------------------------

        # Create combo box that lists the available system fonts
        self.font_combo = QtGui.QComboBox()
        self.font_list = QtGui.QFontDatabase().families()
        self.font_combo.addItems(self.font_list)
        self.font_combo.setCurrentIndex(self.font_list.index(QtGui.QFont().defaultFamily()))

        # Create combo box for default font sizes
        self.size_combo = QtGui.QComboBox()
        self.size_combo.setMaximumWidth(60)
        self.font_size = [6, 8, 12, 16, 20, 26, 32, 48, 72]
        for size in self.font_size:
            self.size_combo.addItem(str(size))

        # Create the font style buttons; bold, italic, underline, font, and color
        bold_btn = add_tool_button(text="B", bold=True)
        italic_btn = add_tool_button(text="I", italic=True)
        underline_btn = add_tool_button(text="U", underline=True)
        font_btn = add_tool_button(icon_name="text.png")
        color_btn = add_tool_button(icon_name="colorProfile.png")

        # Create the text box for user input
        self.text_box = QtGui.QTextEdit()
        self.text_box.setFontFamily(self.font_combo.currentText())
        self.text_box.setFontPointSize(8)

        # Create a label to display available characters left to type in text box
        self.text_label = QtGui.QLabel()
        self.text_label.setText("140 characters left")
        self.text_label.setAlignment(QtCore.Qt.AlignRight)

        # --------------------------------------------------------------------------------------------------------------
        # Setup UI layout
        # --------------------------------------------------------------------------------------------------------------

        # Create main widget and layout
        main_widget = QtGui.QWidget()
        self.setCentralWidget(main_widget)
        main_layout = QtGui.QVBoxLayout()
        main_widget.setLayout(main_layout)

        # Setup the font layout widget and add necessary widgets
        font_layout = QtGui.QHBoxLayout()
        for widget in [self.font_combo, self.size_combo, bold_btn, italic_btn, underline_btn, font_btn, color_btn]:
            font_layout.addWidget(widget)
        main_layout.addLayout(font_layout)
        main_layout.addWidget(self.text_box)
        main_layout.addWidget(self.text_label)

        # --------------------------------------------------------------------------------------------------------------
        # Create the menu bar
        # --------------------------------------------------------------------------------------------------------------

        # Create the menu bar
        menu_bar = QtGui.QMenuBar()
        self.setMenuBar(menu_bar)

        # Create the menus
        menu_file = QtGui.QMenu("File")
        menu_edit = QtGui.QMenu("Edit")
        menu_bar.addMenu(menu_file)
        menu_bar.addMenu(menu_edit)

        # Create actions and add them to meu
        action_new = QtGui.QAction("New", menu_file)
        action_new.triggered.connect(self.text_box.clear)
        action_close = QtGui.QAction("Close", menu_file)
        action_close.triggered.connect(self.close)
        menu_file.addActions([action_new, action_close])
        action_select_all = QtGui.QAction("Select All", menu_edit)
        action_select_all.triggered.connect(self.text_box.selectAll)
        menu_edit.addAction(action_select_all)

        # --------------------------------------------------------------------------------------------------------------
        # Create the status bar
        # --------------------------------------------------------------------------------------------------------------
        # Create the status bar
        self.status_bar = QtGui.QStatusBar()
        self.setStatusBar(self.status_bar)
        self.status_bar.showMessage(INIT_MESSAGE)

        # --------------------------------------------------------------------------------------------------------------
        # Setup widget connections
        # --------------------------------------------------------------------------------------------------------------
        self.text_box.textChanged.connect(self.text_changed)
        self.text_box.cursorPositionChanged.connect(self.cursor_changed)
        self.font_combo.currentIndexChanged.connect(lambda: self.set_font(font=True))
        self.size_combo.currentIndexChanged.connect(lambda: self.set_font(size=True))
        color_btn.clicked.connect(self.show_color_dialog)
        font_btn.clicked.connect(self.show_font_dialog)
        bold_btn.clicked.connect(lambda: self.set_font(bold=True))
        italic_btn.clicked.connect(lambda: self.set_font(italic=True))
        underline_btn.clicked.connect(lambda: self.set_font(underline=True))

    def set_font(self, font=False, size=False, bold=False, italic=False, underline=False):
        """
        Sets the font style of the selected or current text in the text box
        :param font: (bool) set the font family
        :param size: (bool) set the font size
        :param bold: (bool) set the font to bold
        :param italic: (bool) set the font to italic
        :param underline: (bool) set the font to underline
        :return:
        """
        if font:
            family = self.font_combo.currentText()
            self.text_box.setFontFamily(family)

        if size:
            font_size = self.size_combo.currentText()
            self.text_box.setFontPointSize(int(font_size))

        if bold:
            if self.text_box.fontWeight() == 50:
                self.text_box.setFontWeight(75)
                self.status_bar.showMessage("Current font style set to Bold.")
            else:
                self.text_box.setFontWeight(50)

        if italic:
            if self.text_box.fontItalic():
                self.text_box.setFontItalic(False)
            else:
                self.text_box.setFontItalic(True)
                self.status_bar.showMessage("Current font style set to Italic.")

        if underline:
            if self.text_box.fontUnderline():
                self.text_box.setFontUnderline(False)
            else:
                self.text_box.setFontUnderline(True)
                self.status_bar.showMessage("Current font style set to Underline.")

        self.text_box.setFocus()

    def show_color_dialog(self):
        """
        Creates a QT color dialog window
        :return:
        """
        color = QtGui.QColorDialog.getColor()
        if color.isValid():
            self.text_box.setTextColor(color)

    def show_font_dialog(self):
        """
        Creates a QT font dialog window
        :return:
        """
        font, ok = QtGui.QFontDialog.getFont()
        if ok:
            self.text_box.setCurrentFont(font)

    def text_changed(self, limit=140):
        """
        Runs whenever the text box's text has changed to check whether it has reached the input character limit
        :param limit:
        :return:
        """
        text = self.text_box.toPlainText()
        cursor = self.text_box.textCursor()

        # Delete previous character if it has reached the limit
        if len(text) > limit:
            cursor.deletePreviousChar()
            self.status_bar.showMessage("Limit reached")
        else:
            self.text_label.setText("{} characters left".format(limit - len(text)))
            self.status_bar.showMessage(INIT_MESSAGE)

        self.text_box.setTextCursor(cursor)

    def cursor_changed(self):
        """
        Runs when the text box's cursor has changed and sets the font and size combo boxes values
        :return:
        """
        # Get current font and size
        font = self.text_box.fontFamily()
        size = int(self.text_box.fontPointSize())

        # Update font and size combo box index; block the signals first so it doesn't update the text
        if font:
            self.font_combo.blockSignals(True)
            self.font_combo.setCurrentIndex(self.font_list.index(font))
            self.font_combo.blockSignals(False)
        if size:
            if self.size_combo.findText(str(size)) != -1:
                self.size_combo.blockSignals(True)
                self.size_combo.setCurrentIndex(self.font_size.index(size))
                self.size_combo.blockSignals(False)
        else:
            self.size_combo.setCurrentIndex(1)

    def closeEvent(self, *args, **kwargs):
        """
        Save settings when window is closed
        :return:
        """
        settings = QtCore.QSettings("Weta", "Challenge")
        settings.setValue("geometry", self.saveGeometry())
        settings.setValue("text", self.text_box.toHtml())

    def showEvent(self, *args, **kwargs):
        """
        Load settings when window is shown
        :return:
        """
        settings = QtCore.QSettings("Weta", "Challenge")
        self.restoreGeometry(settings.value("geometry"))
        self.text_box.setHtml(settings.value("text"))
        self.text_box.setFocus()
        cursor = self.text_box.textCursor()
        cursor.movePosition(QtGui.QTextCursor.End)
        self.text_box.setTextCursor(cursor)


def add_tool_button(text=None, icon_name=None, width=20, height=20, font_size=12,
                    bold=False, italic=False, underline=False):
    """
    Creates a QToolButton to easily set its parameters
    :param text: (str) text to display
    :param icon_name: (str) name of the icon to use for the button
    :param width: (int) icon width
    :param height: (int) icon height
    :param font_size: (int) font size
    :param bold: (bool) whether to set the font to bold
    :param italic: (bool) whether to set the font to italic
    :param underline: (bool) whether to set the font to underlined
    :return:
    """
    # Create the widget
    button = QtGui.QToolButton()
    button.setAutoRaise(True)

    # Set the text
    if text:
        button.setText(text)

    # Set the button's icon
    if icon_name:
        icon = QtGui.QIcon()
        icon.addPixmap(QtGui.QPixmap(os.path.join(ICON_PATH, icon_name)),
                       QtGui.QIcon.Normal,
                       QtGui.QIcon.Off)
        button.setIcon(icon)
        button.setIconSize(QtCore.QSize(width, height))

    # Set the font style
    if any([bold, italic, underline]):
        font = QtGui.QFont()
        font.setPointSize(font_size)
        font.setBold(bold)
        font.setItalic(italic)
        font.setUnderline(underline)
        if bold:
            font.setWeight(75)
        button.setFont(font)

    return button
